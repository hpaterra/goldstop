﻿Public Class EffectivenessAudit
    Dim _auditeeName As String
    Dim _auditeeDate As Date
    Dim _auditorName As String
    Dim _auditorDate As Date
    Dim _caseNumber As String
    Dim AnswersTA As New GoldStopDataSetTableAdapters.MRP_Effectiveness___AnswersTableAdapter
    Dim _audRow As GoldStopDataSet._MRP_Effectiveness___AuditsRow
#Region "Properties"
    Public Property AuditeeName
        Get
            Return _auditeeName
        End Get
        Set(value)
            _auditeeName = value
        End Set
    End Property

    Public Property AuditeeDate
        Get
            Return _auditeeDate
        End Get
        Set(value)
            _auditeeDate = value
        End Set
    End Property
    Public Property AuditorName
        Get
            Return _auditorName
        End Get
        Set(value)
            _auditorName = value
        End Set
    End Property
    Public Property AuditorDate
        Get
            Return _auditorDate
        End Get
        Set(value)
            _auditorDate = value
        End Set
    End Property
    Public Property CaseNumber
        Get
            Return _caseNumber
        End Get
        Set(value)
            _caseNumber = value
        End Set
    End Property
#End Region
    Public Sub New(audRow As GoldStopDataSet._MRP_Effectiveness___AuditsRow)
        _audRow = audRow
        With _audRow
            .AuditeeName = _auditeeName
            .AuditeeDate = _auditeeDate
            .AuditorName = _auditorName
            .AuditorDate = _auditorDate
        End With
    End Sub
    Public Function NumAnswered() As Integer
        Return AnswersTA.GetDataByAuditID(_audRow.Id).Count
    End Function
End Class
