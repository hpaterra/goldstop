﻿Class EffectivenessLanding
    Dim user As GoldStopDataSet.CredentialsRow
    Dim credTA As New GoldStopDataSetTableAdapters.CredentialsTableAdapter
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        My.User.InitializeWithWindowsUser()
        Dim username = My.User.Name.Substring(4)
        If credTA.GetDataByUsername(username).Count = 1 Then
            user = credTA.GetDataByUsername(username)(0)
            WelcomeLabel.Text = "Welcome " & user.First & " " & user.Last & "!" & vbNewLine & "Select an in-progress audit below or fill out the fields to the right to start a new one!"
        Else
            WelcomeLabel.Text = "You don't have permission to create effectiveness audits. If you believe this is an error, please contact Harry Paterra"
        End If
    End Sub
End Class
